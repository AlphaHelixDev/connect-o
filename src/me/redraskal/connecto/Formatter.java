package me.redraskal.connecto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import me.redraskal.connecto.api.incoming.Source;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class Formatter {

    private static final Gson GSON = new GsonBuilder().create();

    public static Gson getGson() {
        return GSON;
    }

    /**
     * Formats a message to be sent.
     * @param channel
     * @param message
     * @param targetServer
     * @param source
     * @return
     */
    public static String formatMessage(String channel, String message, String targetServer, Source source) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("channel", channel);
        jsonObject.addProperty("message", message);
        jsonObject.addProperty("server", targetServer);
        jsonObject.addProperty("source", source.getID());
        return GSON.toJson(jsonObject);
    }

    /**
     * Formats a message to be sent.
     * @param channel
     * @param message
     * @param targetServer
     * @param serverSource
     * @param source
     * @return
     */
    public static String formatMessage(String channel, String message, String targetServer, String serverSource, Source source) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("channel", channel);
        jsonObject.addProperty("message", message);
        jsonObject.addProperty("targetServer", targetServer);
        jsonObject.addProperty("serverSource", serverSource);
        jsonObject.addProperty("source", source.getID());
        return GSON.toJson(jsonObject);
    }

    /**
     * Parses a message received by the server.
     * @param message
     * @return
     */
    public static JsonObject parseMessage(String message) {
        return GSON.fromJson(message, JsonObject.class);
    }
}
