package me.redraskal.connecto;

import me.redraskal.connecto.api.ConnectoFactory;
import me.redraskal.connecto.api.MiniPlugin;
import me.redraskal.connecto.listener.ProxyListener;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class Bungee extends Plugin implements MiniPlugin {

    private Bungee instance;
    private ConnectoFactory connectoFactory;

    @Override
    public void onEnable() {
        instance = this;
        connectoFactory = new ConnectoFactory(instance);
        new ProxyListener(instance);
    }

    @Override
    public void onDisable() {
        this.connectoFactory.deleteAll();
    }

    @Override
    public boolean isBungeecord() {
        return true;
    }

    @Override
    public ConnectoFactory getFactory() {
        return this.connectoFactory;
    }

    @Override
    public Object getPlugin() {
        return this;
    }

    @Override
    public String getBungeeServer() {
        return "BungeeCord";
    }
}