package me.redraskal.connecto.listener;

import com.google.gson.JsonObject;
import me.redraskal.connecto.Formatter;
import me.redraskal.connecto.api.MiniPlugin;
import me.redraskal.connecto.api.incoming.Source;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Arrays;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class SpigotListener implements PluginMessageListener {

    private MiniPlugin instance;

    /**
     * Registers the listener for the specified plugin.
     * @param spigotPlugin
     */
    public SpigotListener(MiniPlugin spigotPlugin) {
        this.instance = spigotPlugin;
        Bukkit.getMessenger().registerOutgoingPluginChannel((org.bukkit.plugin.Plugin) instance.getPlugin(), "connecto");
        Bukkit.getMessenger().registerIncomingPluginChannel((org.bukkit.plugin.Plugin) instance.getPlugin(), "connecto", this);
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        ((JavaPlugin) instance).getLogger().info("Proxy ==> Spigot: " + channel + ", " + Arrays.toString(message));
        if(!channel.equalsIgnoreCase("connecto"))
            return;
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        try {
            String decode = in.readUTF();
            if (!decode.isEmpty()) {
                // CHANNEL / INCOMING OBJECT / SOURCE
                JsonObject jsonObject = Formatter.parseMessage(decode);
                ((JavaPlugin) instance).getLogger().info("Proxy ==> Spigot: " + Formatter.getGson().toJson(jsonObject));
                instance.getFactory().registerHandler(
                        jsonObject.getAsJsonObject("channel").getAsString(),
                        jsonObject.get("message"),
                        jsonObject.getAsJsonObject("serverSource").getAsString(),
                        Source.fromID(((Long) jsonObject.getAsJsonObject("source").getAsLong()).intValue()));
            }
        } catch (Exception ignored) {
        }
    }
}
