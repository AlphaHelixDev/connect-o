package me.redraskal.connecto.listener;

import com.google.gson.JsonObject;
import me.redraskal.connecto.Formatter;
import me.redraskal.connecto.api.MiniPlugin;
import me.redraskal.connecto.api.incoming.Source;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.io.*;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class ProxyListener implements Listener {

    private MiniPlugin instance;

    /**
     * Registers the listener for the specified plugin.
     * @param proxyPlugin
     */
    public ProxyListener(MiniPlugin proxyPlugin) {
        this.instance = proxyPlugin;
        ProxyServer.getInstance().registerChannel("connecto");
        ProxyServer.getInstance().getPluginManager().registerListener(((Plugin) proxyPlugin.getPlugin()), this);
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent pluginMessageEvent) {
        ProxyServer.getInstance().getLogger().info("Spigot ==> Proxy: " + pluginMessageEvent.getTag() + ", " + pluginMessageEvent.getData());
        if(!pluginMessageEvent.getTag().equalsIgnoreCase("connecto"))
            return;
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(pluginMessageEvent.getData()));
        try {
            String decode = in.readUTF();
            if (!decode.isEmpty()) {
                // CHANNEL / INCOMING OBJECT / SOURCE
                JsonObject jsonObject = Formatter.parseMessage(decode);
                ProxyServer.getInstance().getLogger().info("Spigot ==> Proxy: " + Formatter.getGson().toJson(jsonObject));

                if (jsonObject.has("serverSource")) {
                    jsonObject.remove("serverSource");
                    jsonObject.addProperty("serverSource", ProxyServer.getInstance().getPlayer(pluginMessageEvent.getReceiver().toString()).getServer().getInfo().getName());
                }

                if (jsonObject.getAsJsonObject("targetServer").getAsString().equalsIgnoreCase("BungeeCord")) {
                    instance.getFactory().registerHandler(
                            jsonObject.getAsJsonObject("channel").getAsString(),
                            jsonObject.get("message").getAsString(),
                            ProxyServer.getInstance().getPlayer(pluginMessageEvent.getReceiver().toString()).getServer().getInfo().getName(),
                            Source.fromID(((Long) jsonObject.getAsJsonObject("source").getAsLong()).intValue()));
                } else {
                    // SPIGOT_PLUGIN => BungeeCord (relay node) => ANOTHER SPIGOT_PLUGIN
                    if ((jsonObject.getAsJsonObject("targetServer").getAsString()).equalsIgnoreCase("ALL")) {
                       for(ServerInfo allServers : ((Plugin) instance).getProxy().getServers().values()) {
                           this.sendData(Formatter.getGson().toJson(jsonObject),
                                   allServers);
                       }
                   } else {
                        this.sendData(Formatter.getGson().toJson(jsonObject),
                                ProxyServer.getInstance().getServerInfo(jsonObject.getAsJsonObject("targetServer").getAsString()));
                   }
                }
            }
        } catch (Exception e) {}
    }

    /**
     * The fancy backend method of sending data over to Spigot servers.
     * @param message
     * @param serverInfo
     * @throws IOException
     */
    public void sendData(String message, ServerInfo serverInfo) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        out.writeUTF(message);
        serverInfo.sendData("connecto", stream.toByteArray());
    }
}
