package me.redraskal.connecto.api.incoming;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public abstract class IncomingChannel {

    /**
     * The event called when data is received by the server.
     * @param incomingObject
     * @param serverSource
     * @param source
     */
    public abstract void handle(Object incomingObject, String serverSource, Source source);
}