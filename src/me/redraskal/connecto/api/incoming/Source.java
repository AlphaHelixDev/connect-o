package me.redraskal.connecto.api.incoming;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public enum Source {

    PROXY(0),
    PLUGIN(1);

    private int id;

    Source(int id) {
        this.id = id;
    }

    public static Source fromID(int id) {
        for(Source source : Source.values()) {
            if(source.getID() == id) {
                return source;
            }
        }
        return null;
    }

    public int getID() {
        return this.id;
    }
}