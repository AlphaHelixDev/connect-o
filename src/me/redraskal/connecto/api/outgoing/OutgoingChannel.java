package me.redraskal.connecto.api.outgoing;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public interface OutgoingChannel {

    /**
     * Sends data to the specified server. (Use BungeeCord for the proxy)
     * @param data
     * @param server
     * @return
     */
    boolean send(Object data, String server);
}