package me.redraskal.connecto.api.outgoing;

import me.redraskal.connecto.Formatter;
import me.redraskal.connecto.api.Connecto;
import me.redraskal.connecto.api.incoming.Source;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class OutgoingProxyChannel implements OutgoingChannel {

    private Connecto instance;

    /**
     * Registers an outgoing channel.
     * @param instance
     */
    public OutgoingProxyChannel(Connecto instance) {
        this.instance = instance;
    }

    public boolean send(Object data, String server) {
        if(!this.instance.getMiniPlugin().isBungeecord()
                || server.equalsIgnoreCase("BungeeCord"))
            return false;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        ServerInfo serverInfo = ((Plugin) instance.getMiniPlugin()).getProxy().getServerInfo(server);
        String encodedJson = Formatter.formatMessage(instance.getChannel(), (String) data, server, "BungeeCord", Source.PROXY);
        ((Plugin) instance.getMiniPlugin()).getLogger().info("Proxy ==> Spigot: " + encodedJson);
        try {
            out.writeUTF(encodedJson);
            serverInfo.sendData("connecto", stream.toByteArray());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
