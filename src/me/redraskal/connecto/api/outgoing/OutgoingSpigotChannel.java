package me.redraskal.connecto.api.outgoing;

import me.redraskal.connecto.Formatter;
import me.redraskal.connecto.api.Connecto;
import me.redraskal.connecto.api.incoming.Source;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class OutgoingSpigotChannel implements OutgoingChannel {

    private Connecto instance;

    /**
     * Registers an outgoing channel.
     * @param instance
     */
    public OutgoingSpigotChannel(Connecto instance) {
        this.instance = instance;
    }

    public boolean send(Object data, String server) {
        if(this.instance.getMiniPlugin().isBungeecord()
                || this.randomPlayer() == null)
            return false;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        String encodedJson = Formatter.formatMessage(instance.getChannel(), (String) data, server, instance.getMiniPlugin().getBungeeServer(), Source.PLUGIN);
        ((JavaPlugin) instance.getMiniPlugin()).getLogger().info("Spigot ==> Proxy: " + encodedJson);
        try {
            out.writeUTF(encodedJson);
            this.randomPlayer().sendPluginMessage((org.bukkit.plugin.Plugin) instance.getMiniPlugin(), "connecto", stream.toByteArray());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Returns a random player online,
     * null if no one is available.
     * @return
     */
    private Player randomPlayer() {
        if(Bukkit.getOnlinePlayers().size() <= 0)
            return null;
        Player[] players = Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()]);
        return players[ThreadLocalRandom.current().nextInt(players.length)];
    }
}
