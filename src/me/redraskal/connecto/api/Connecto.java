package me.redraskal.connecto.api;

import me.redraskal.connecto.api.incoming.IncomingChannel;
import me.redraskal.connecto.api.outgoing.OutgoingChannel;
import me.redraskal.connecto.api.outgoing.OutgoingProxyChannel;
import me.redraskal.connecto.api.outgoing.OutgoingSpigotChannel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class Connecto {

    private String channel;
    private MiniPlugin miniPlugin;
    private List<IncomingChannel> handlers = new ArrayList<IncomingChannel>();

    /**
     * The system that governs a specific incoming.
     * @param channel
     * @param miniPlugin
     */
    Connecto(String channel, MiniPlugin miniPlugin) {
        this.channel = channel;
        this.miniPlugin = miniPlugin;
    }

    /**
     * Returns a new OutgoingChannel instance for the channel.
     * @return
     */
    public OutgoingChannel getOutgoingChannel() {
        if(this.getMiniPlugin().isBungeecord()) {
            return new OutgoingProxyChannel(this);
        } else {
            return new OutgoingSpigotChannel(this);
        }
    }

    /**
     * Registers a handler for the incoming.
     * @param incomingChannel
     */
    public void registerHandler(IncomingChannel incomingChannel) {
        this.handlers.add(incomingChannel);
    }

    /**
     * Returns an array of the registered handlers.
     * @return
     */
    public IncomingChannel[] getHandlerList() {
        return this.handlers.toArray(new IncomingChannel[this.handlers.size()]);
    }

    /**
     * Returns the incoming name.
     * @return
     */
    public String getChannel() {
        return this.channel;
    }

    /**
     * Returns the MiniPlugin instance.
     * @return
     */
    public MiniPlugin getMiniPlugin() {
        return this.miniPlugin;
    }
}
