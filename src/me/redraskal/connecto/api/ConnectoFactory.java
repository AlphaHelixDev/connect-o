package me.redraskal.connecto.api;

import me.redraskal.connecto.api.incoming.IncomingChannel;
import me.redraskal.connecto.api.incoming.Source;

import java.util.*;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class ConnectoFactory {

    private static List<ConnectoFactory> globalInstances = new ArrayList<ConnectoFactory>();

    private MiniPlugin miniPlugin;
    private Map<String, Connecto> instances = new HashMap<String, Connecto>();
    private UUID factoryUUID;

    /**
     * A factory in which contains the channels registered with the server.
     * @param miniPlugin
     */
    public ConnectoFactory(MiniPlugin miniPlugin) {
        this.miniPlugin = miniPlugin;
        this.factoryUUID = UUID.randomUUID();
        globalInstances.add(this);
    }

    /**
     * Creates a Connecto instance that governs the specified incoming.
     * @param channel
     * @return
     */
    public Connecto createChannel(String channel) {
        if(channelExists(channel))
            return this.getChannel(channel);
        this.instances.put(channel, new Connecto(channel, this.miniPlugin));
        return this.getChannel(channel);
    }

    /**
     * Returns true if the specified incoming was deleted successfully.
     * @param channel
     * @return
     */
    public boolean deleteChannel(String channel) {
        if(!channelExists(channel))
            return false;
        this.instances.remove(channel);
        return true;
    }

    /**
     * Returns a incoming with the specified name.
     * @param channel
     * @return
     */
    public Connecto getChannel(String channel) {
        if(!channelExists(channel))
            return null;
        return this.instances.get(channel);
    }

    /**
     * Returns true if the specified incoming exists.
     * @param channel
     * @return
     */
    public boolean channelExists(String channel) {
        return instances.containsKey(channel);
    }

    /**
     * Deletes all of the channels initialized.
     */
    public void deleteAll() {
        this.instances.clear();
    }

    public UUID getFactoryUUID() {
        return this.factoryUUID;
    }

    public class GlobalHandler {

        /**
         * Handles an incoming channel's data.
         * @param channel
         * @param incomingObject
         * @param serverSource
         * @param source
         */
        public GlobalHandler(String channel, Object incomingObject, String serverSource, Source source) {
            for(ConnectoFactory instance : globalInstances) {
                for(Map.Entry<String, Connecto> counterPart : instance.instances.entrySet()) {
                    if(counterPart.getKey().equalsIgnoreCase(channel)) {
                        for(IncomingChannel incomingChannel : counterPart.getValue().getHandlerList()) {
                            incomingChannel.handle(incomingObject, serverSource, source);
                        }
                    }
                }
            }
        }
    }

    /**
     * Registers a new GlobalHandler.
     * @param channel
     * @param incomingObject
     * @param source
     */
    public void registerHandler(String channel, Object incomingObject, String serverSource, Source source) {
        new GlobalHandler(channel, incomingObject, serverSource, source);
    }
}
