package me.redraskal.connecto.api;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public interface MiniPlugin {

    /**
     * Returns true if the specified plugin is run with BungeeCord.
     * @return
     */
    public boolean isBungeecord();

    /**
     * Returns the ConnectoFactory instance for the plugin.
     * @return
     */
    public ConnectoFactory getFactory();

    /**
     * Returns the plugin instance;
     * @return
     */
    public Object getPlugin();

    /**
     * Returns the current BungeeCord server.
     * @return
     */
    public String getBungeeServer();
}