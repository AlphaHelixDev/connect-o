package me.redraskal.connecto;

import me.redraskal.connecto.api.ConnectoFactory;
import me.redraskal.connecto.api.MiniPlugin;
import me.redraskal.connecto.listener.SpigotListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class Spigot extends JavaPlugin implements MiniPlugin {

    private Spigot instance;
    private ConnectoFactory connectoFactory;

    @Override
    public void onEnable() {
        instance = this;
        connectoFactory = new ConnectoFactory(instance);
        new SpigotListener(instance);
    }

    @Override
    public void onDisable() {
        this.connectoFactory.deleteAll();
    }

    @Override
    public boolean isBungeecord() {
        return false;
    }

    @Override
    public ConnectoFactory getFactory() {
        return this.connectoFactory;
    }

    @Override
    public Object getPlugin() {
        return this;
    }

    @Override
    public String getBungeeServer() {
        return Bukkit.getServerName();
    }
}
