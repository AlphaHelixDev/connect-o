package example;

import me.redraskal.connecto.api.Connecto;
import me.redraskal.connecto.api.ConnectoFactory;
import me.redraskal.connecto.api.MiniPlugin;
import me.redraskal.connecto.api.incoming.IncomingChannel;
import me.redraskal.connecto.api.incoming.Source;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class SpigotServer extends JavaPlugin implements Listener {

    /**
     * We're going to register a channel, and send a test piece of info to BungeeCord.
     * The classic ping pong!
     */
    public void onEnable() {
        // We're fetching the "factory"!
        // The ConnectoFactory instance will handle all the channel instances being listened into by the server.
        ConnectoFactory connectoFactory = this.getMiniPlugin().getFactory();

        // Let's make a channel called "test"
        Connecto connecto = connectoFactory.createChannel("test");
        Connecto connectoChat = connectoFactory.createChannel("chat");
        // And would you look at that? We're already listening into the channel "test".
        // Isn't that fancy?
        // ikright, no work at all. I'm telling ya!

        // Now let's listen for the server's response before we send our message!
        connecto.registerHandler(new IncomingChannel() {
            @Override
            public void handle(Object incomingObject, String serverSource, Source source) {
                Bukkit.broadcastMessage("Incoming object! [" + incomingObject + ", " + serverSource + ", " + source.toString() + "]");
                if(incomingObject.equals("pong")) {
                    Bukkit.broadcastMessage("OMG we got a response from the proxy! (source: " + serverSource + ":" + source.toString() + ")");
                }
            }
        });

        connectoChat.registerHandler(new IncomingChannel() {
            @Override
            public void handle(Object incomingObject, String serverSource, Source source) {
                Bukkit.broadcastMessage((String) incomingObject);
            }
        });


        this.getServer().getPluginManager().registerEvents(this, this);
        Bukkit.broadcastMessage("Factory UUID: " + connectoFactory.getFactoryUUID().toString());

        // Now the code should work!
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent asyncPlayerChatEvent) {
        this.getMiniPlugin().getFactory().getChannel("chat").getOutgoingChannel().send(asyncPlayerChatEvent.getMessage(), "ALL");
        asyncPlayerChatEvent.setCancelled(true);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("test")) {
            Bukkit.broadcastMessage("We're going to send a message to the proxy now!");
            Bukkit.broadcastMessage("Factory UUID: " + this.getMiniPlugin().getFactory().getFactoryUUID().toString());
            if(this.getMiniPlugin().getFactory() != null)
                Bukkit.broadcastMessage("Debug1!");
            Bukkit.broadcastMessage("Channel debug 1: " + this.getMiniPlugin().getFactory().channelExists("test"));
            if(this.getMiniPlugin().getFactory().getChannel("test") != null)
                Bukkit.broadcastMessage("Debug2!");
            if(this.getMiniPlugin().getFactory().getChannel("test").getOutgoingChannel().send("ping", "BungeeCord")) {
                Bukkit.broadcastMessage("We're good!");
            } else {
                Bukkit.broadcastMessage("Something happened.");
            }
            return true;
        }
        return false;
    }

    /**
     * Fancy magical stuffidy-stuff to fetch the MiniPlugin instance.
     * @return
     */
    public MiniPlugin getMiniPlugin() {
        return ((MiniPlugin) this.getServer().getPluginManager().getPlugin("Connecto"));
    }
}
