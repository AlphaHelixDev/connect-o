package example;

import me.redraskal.connecto.api.Connecto;
import me.redraskal.connecto.api.ConnectoFactory;
import me.redraskal.connecto.api.MiniPlugin;
import me.redraskal.connecto.api.incoming.IncomingChannel;
import me.redraskal.connecto.api.incoming.Source;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by Redraskal_2 on 11/12/2016.
 */
public class BungeeCordServer extends Plugin {

    /**
     * We're going to register a channel, and send a test piece of info to BungeeCord.
     * The classic ping pong!
     */
    public void onEnable() {
        // We're fetching the "factory"!
        // The ConnectoFactory instance will handle all the channel instances being listened into by the server.
        ConnectoFactory connectoFactory = this.getMiniPlugin().getFactory();

        // Let's make a channel called "test"
        Connecto connecto = connectoFactory.createChannel("test");
        // And would you look at that? We're already listening into the channel "test".
        // Isn't that fancy?
        // ikright, no work at all. I'm telling ya!

        // Now let's listen for the server's response before we send our message!
        connecto.registerHandler(new IncomingChannel() {
            @Override
            public void handle(Object incomingObject, String serverSource, Source source) {
                if(incomingObject.equals("ping")) {
                    getProxy().getLogger().info("A spigot server is wanting some information!");
                    connecto.getOutgoingChannel().send("pong", serverSource);
                }
            }
        });

        // Now the code should work!
    }

    /**
     * Fancy magical stuffidy-stuff to fetch the MiniPlugin instance.
     * @return
     */
    public MiniPlugin getMiniPlugin() {
        return ((MiniPlugin) this.getProxy().getPluginManager().getPlugin("Connecto"));
    }
}
